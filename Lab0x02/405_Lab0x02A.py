'''
@file       405_Lab0x02A.py
@brief      This file runs the "Think Fast" labpart A.
@details    Using the Nucleo to control the timing, the user will see an LED
            blink on after 2-3 seconds. After the LED has turned on, the user
            must press the blue button as quickly as possible. The LED will 
            then turn off after 1 second, and the user can release whenever.
            The goal is to measure the reaction time of the user over a few
            LED presses.
            *See source code here:* 
@author     Matthew Frost
@date       Originally created on 04/22/21 \n Last modified on 04/22/21
'''

