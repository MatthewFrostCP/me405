'''
@file       changeCalculator.py
@brief      This file computes the change for a certain purchase.
@details    The user will input the quantity of each denomination that they 
            are using to purchase the item, along with the price of the item,
            and the corresponding change will be calculated in terms of the
            quantity of each denomination that will be returned.
            *See source code here:* 
@author     Matthew Frost
@date       Originally created on 04/04/21 \n Last modified on 04/04/21
'''

def getChange(price, payment):
    
    # Define the amount of each denomination in cents
    denominations = [1, 5, 10, 25, 100, 500, 1000, 2000]
    amount = 0
    
    # Compute the payment in terms of cents
    for n in range(len(payment)):
        amount += payment[n]*denominations[n]
        
    print('Amount in cents is: {:}'.format(amount))
    
    # Compute necessary change
    if amount < price:
        print('Insufficient funds, please add more money')
    else:
        # Compute the change needed in terms of cents
        change = amount - price
        print('Change needed is: {:} cents'.format(change))

        changeToReturn = 8*[0]
        
        for i in range(len(denominations)):
            idx = len(denominations) - i - 1
            changeToReturn[idx], change = divmod(change, denominations[idx])
    
    return changeToReturn


if __name__ == "__main__":

    # Have user input a price in cents
    price = int(input('Enter the price of the item in cents: '))
    print('Price of item is {:} cents'.format(price))
    
    # Below line read inputs from user using map() function 
    payment = list(map(int,input('Enter the quantity of each denomination '
                                 'separated by spaces : ').strip().split()))
    
    # Show the payment as a list of denomination quantities
    print('Payment is: ', payment)
    
    # Compute the change
    changeReturned = getChange(price, payment)
    
    # Show the change as a list of denomination quantities
    print('The amount of change returned is: ' + str(changeReturned))
    
    # Program de-initialization
    print('Thanks for testing out the change calculator. See you next time!')
    
    
    
    
    
  