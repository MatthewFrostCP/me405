'''
@file       changeCalculator.py
@brief      This file computes the change for a certain purchase.
@details    The user will input the quantity of each denomination that they 
            are using to purchase the item, along with the price of the item,
            and the corresponding change will be calculated in terms of the
            quantity of each denomination that will be returned.
            *See source code here:* 
@author     Matthew Frost
@date       Originally created on 04/04/21 \n Last modified on 04/04/21
'''

def getChange(price, payment):
    
    # Define the amount of each denomination in cents
    amountInserted = 0
    
    # Compute the payment in terms of cents
    for n in payment:
        amountInserted += n*payment[n]
        
    print('Amount in cents is: {:}'.format(amountInserted))
    
    # Compute necessary change
    if amountInserted < price:
        print('Insufficient funds, please add more money')
    else:
        # Compute the change needed in terms of cents
        change = amountInserted - price
        print('Change needed is: {:} cents'.format(change))
        
        denomList = list(payment.keys())
        denomList.sort(reverse = True)
        
        changeToReturn = {}
        
        for denomination in denomList:
            changeToReturn[denomination], change = divmod(change, denomination)
    
    return changeToReturn


if __name__ == "__main__":

    # Have user input a price in cents
    price = 1001
    print('Price of item is {:} cents'.format(price))
    
    denomination = {'0':('Penny',1), '1':('Nickel',5), '2':('Dime',10), 
                    '3':('Quarter',25), '4':('One',100), '5':('Five',500),
                    '6':('Ten',1000), '7':('Twenty',2000)}
    
    # Below line read inputs from user using map() function 
    payment = {1:1, 5:3, 10:0, 25:2, 100:1, 500:0, 1000:0, 2000:1}
    
    # Show the payment as a list of denomination quantities
    print('Payment is: {:}'.format(str(payment)))
    
    # Compute the change
    changeReturned = getChange(price, payment)
    
    # Show the change as a list of denomination quantities
    print('The amount of change returned is: {:}'.format(str(changeReturned)))
    
    # Program de-initialization
    print('Thanks for testing out the change calculator. See you next time!')
    
    
    
    
    
  