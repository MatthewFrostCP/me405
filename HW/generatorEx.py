def fibSequence(numItems):
    
    # Find seed #0 first
    f0 = 0
    yield(f0)
    
    # Find seed #1 
    f1 = 1
    yield(f1)
    
    # Find the general Fibonacci term
    counter = 2
    while(counter < numItems):
        f2 = f1 + f0 # Calculate new Fibonacci number
        f0 = f1      # Keep track of new f0
        f1 = f2      # Keep track of new f1
        counter += 1 # Update counter for next iteration
        yield(f2)
    

if __name__ == "__main__":
    mySequence = fibSequence(10)
    
    # Manual way to run generator
    # print(next(mySequence))
    # print(next(mySequence))
    # print(next(mySequence))
    # print(next(mySequence))
    
    for fibNum in mySequence:
        print(fibNum)
