'''
@file       405_Lab0x01.py
@brief      This file runs the VendoTron FSM.
@details    The user will input the quantity of each denomination that they 
            are using to purchase the item, along with the price of the item,
            and the required drink, and several operations will be utilitized
            to simulate the workings of an actual vending machine.
            *See source code here:* https://bitbucket.org/MatthewFrostCP/me405/src/master/Lab0x01/Lab0x01.py
@image      html ME405_Lab1_FSM.jpg " " width = 50%
@details    \n <CENTER> **Figure 1.** FSM for VendoTron </CENTER> \n
@author     Matthew Frost
@date       Originally created on 04/08/21 \n Last modified on 04/08/21
'''

import keyboard
import time

def kb_cb(key):
    ''' 
    @brief   Callback function inside keyboard module.
    @details When keyboard is pressed, it automatically gets sent to the
             callback function to assign a variable the letter pressed. 
    @param   key The key that was pressed to trigger the callback function.
    '''
    global last_key
    last_key = key.name

def getChange(price, balance):
    ''' 
    @brief   This file computes the change for a certain purchase.
    @details The user will input the quantity of each denomination that they 
             are using to purchase the item, along with the price of the item,
             and the corresponding change will be calculated in terms of the
             quantity of each denomination that will be returned.
    @param   price The price of the selected soda.
    @param   balance A dictionary of the current balance.
    '''
    
    ## @brief Computes the balances in units of cents
    balanceInCents = convertToCents(balance) # Convert balance into cents
    print('\nAmount in cents is: {:}'.format(balanceInCents))

    ## @brief The change required in cents
    change = balanceInCents - price # Compute the change needed
    print('\nChange needed is: {:} cents'.format(change))
    
    ## @brief A lost of the denomination values
    denomList = list(balance.keys()) # create a list of the denomination values
    denomList.sort(reverse = True)   # flip the list to descend
    
    ## @brief An empty dictionary for change to return
    changeToReturn = {}
    
    for i in denomList:  # Find the quantity of each denomination to return
        changeToReturn[i], change = divmod(change, i)
    return changeToReturn

def convertToCents(_moneyDict):
    ''' 
    @brief   Converts denominations and quantities to cents.
    @details When a dictionary of denominations and values are inputted, the
             corresponding value in cents is outputted.
    '''
    _moneyCents = 0
    
    for n in _moneyDict: # Compute the balance in terms of cents
        _moneyCents += n*_moneyDict[n]
    return _moneyCents

def vendoTronTask(debugFlag, payment):
    ''' 
    @brief   Generator used to hold the FSM for the vending machine.
    @details This implements the finite state machine used to simulate the
             vending machine. This will handle all the keyboard inputs, timing
             issues, and is in charge of calling other functions to calculate
             change and balance.
    @param   debugFlag When enabled to True, more print commands are displayed.
    @param   payment A dictionary with the current balance or payment.
    '''
    state = S0_INIT   # Start in the init state
    ## @brief Assign the payment dictionary to a balance dictionary
    balance = payment 
    global last_key   # Name the last_key function global
    
    while True: 
    
        if state == S0_INIT: # Init state
            print('Welcome to Vendotron, please select from the following options: '
                  '\nIf you are inputting money, press the following: '
                  '\n0 = Penny \n1 = Nickel \n2 = Dime \n3 = Quarter \n4 = $1' 
                  '\n5 = $5 \n6 = $10 \n7 = $20'
                  '\nIf you want to perform a command, press the following: '
                  '\ne = Eject money \nc = Cuke \np = Popsi \ns = Spryte \nd = Dr. Pepper')
            
            ## @brief The start time for the elapsed timer
            startTime = time.monotonic() # Record the initial start time
            if debugFlag == True:
                print('Start time is at: {:}'.format(startTime))
            state = S1_WAIT_FOR_ACTION # Increment instantly to wait for input
            
        elif state == S1_WAIT_FOR_ACTION: # Wait for action
            ## @brief The current time of the loop
            currentTime = time.monotonic() 
            ## @brief The time since starting
            elapsedTime = currentTime - startTime 
            if debugFlag == True:
                print('Elapsed time is {:}'.format(elapsedTime))
            
            # If a key is pressed to signal a coin insert, recognize it here
            if last_key in denomination:
                state = S2_DISPLAY_BALANCE
                startTime = currentTime
            
            # If a key is pressed to signal a soda, recognize it here
            elif last_key in sodas:
                state = S3_DO_TRANSACTION
                startTime = currentTime
            
            # Used to exit the program
            elif last_key == 'q':
                if debugFlag == True:
                    print("\n'q' detected. Quitting...")
                keyboard.unhook_all ()
                state = S6_THANK_USER
                print('\nThank you for purchasing! Please come again')
                last_key = None
            
            # Pressed in order to eject current balance
            elif last_key == 'e':
                if debugFlag == True:
                    print("\n'e' detected. Prepping to eject money")
                state = S5_RETURN_CHANGE
                last_key = None
            
            # If there's no activity for 10 seconds, prompt user to continue
            if elapsedTime > 10:
                state = S7_TIME_OUT
            
        elif state == S2_DISPLAY_BALANCE: # Display balance
            # Figure out which coin was inserted
            _denominationType, _denominationValue = denomination[last_key]
            print('\nYou have inserted a {:}'.format(_denominationType))
            
            balance[_denominationValue] += 1 # Add the coin to the current balance
            balanceInCents = convertToCents(balance) # Convert balance to cents
            print('\nBalance is {:}, \nwhich is {:} cents'.format(str(balance), balanceInCents))
            
            last_key = None            # Reset last_key
            state = S1_WAIT_FOR_ACTION # wait for more inputs
            
        elif state == S3_DO_TRANSACTION:
            # Figure out which soda was selected and its price
            _sodaName, price = sodas[last_key]
            print('\nYou have asked for a {:}'.format(_sodaName))
            print('The price of the soda is {:}'.format(price))
            
            balanceInCents = convertToCents(balance) # Convert balance to cents
            
            # Determine if user can purchase the soda
            if balanceInCents >= price:
                balance = getChange(price, balance) # Recalculate balance after purchase
                print('\nHere is your {:}'.format(_sodaName))

                change = convertToCents(balance) # Convert balance to cents
                
                if change == 0:
                    print('\nNo remaining balance, thanks for coming!')
                    state = S6_THANK_USER
                else:
                    print('\nYou still have some remaining balance!')
                    print('\nWould you like to purchase another drink? Press y or n')
                    state = S4_QUERY_FOR_DRINK
            else:
                print('\nInsufficient funds, please insert more')
                state = S1_WAIT_FOR_ACTION
            last_key = None # Reset last_key
                
        elif state == S4_QUERY_FOR_DRINK:
            # If user presses 'y', allow them to purchase another drink
            if last_key =='y':
                print('\nYou have selected to pick out another soda')
                state = S1_WAIT_FOR_ACTION
            elif last_key =='n':
                print('\nYou have selected to decline the soda')
                state = S5_RETURN_CHANGE
            elif last_key == 'e':
                print("\n'e' detected. Prepping to eject money")
                state = S5_RETURN_CHANGE
            last_key = None # Reset last_key    
            
        elif state == S5_RETURN_CHANGE:
            print('\nHere is your change: {:}, please come again!'.format(str(balance)))
            state = S6_THANK_USER
        
        elif state == S6_THANK_USER:
            pass # A holding spot to end the simulation and not display a message
            
        elif state == S7_TIME_OUT:
            # If user takes more than 10 seconds, remind them to finish transaction
            print('\nSystem timed out. Please finish transaction in a timely manner.')
            startTime = time.monotonic() # Reset clock
            state = S1_WAIT_FOR_ACTION
            
        yield(state)


if __name__ == "__main__":
    
    ## @brief The init state.
    S0_INIT               = 0
    ## @brief Waits for user to press a button.
    S1_WAIT_FOR_ACTION    = 1
    ## @brief Display the current balance.
    S2_DISPLAY_BALANCE    = 2
    ## @brief Computations after a drink is selected.
    S3_DO_TRANSACTION     = 3
    ## @brief Ask user for another drink.
    S4_QUERY_FOR_DRINK    = 4
    ## @brief return change.
    S5_RETURN_CHANGE      = 5
    ## @brief Thank user for buying a drink.
    S6_THANK_USER         = 6
    ## @brief System times out and reminds user to finish.
    S7_TIME_OUT           = 7

    ## @brief   Dictionary of denominations
    #  @details A dictionary of denominations that has the keyboard button as the key, 
    #           and then a tuple for the name and amount of each denomination
    denomination = {'0':('Penny',1), '1':('Nickel',5), '2':('Dime',10), 
                    '3':('Quarter',25), '4':('One',100), '5':('Five',500),
                    '6':('Ten',1000), '7':('Twenty',2000)}
    
    ## @brief   A dictionary of the starting balance, which should be 0
    balance = {1:0, 5:0, 10:0, 25:0, 100:0, 500:0, 1000:0, 2000:0}
    
    ## @brief   Dictionary for soda options
    #  @details A dictionary of soda options that has the keyboard button as the key, 
    #           and then a tuple for the name and price of each soda
    sodas = {'c':('Cuke',100), 'p':('Popsi',120), 's':('Spryte',85), 'd':('Dr. Pupper',110)}
    
    ## @brief A dictionary of the starting change, which should be 0
    change = {1:0, 5:0, 10:0, 25:0, 100:0, 500:0, 1000:0, 2000:0}
    
    ## @brief A list of the possible keyboard commands that could be pressed
    Keys = ["0","1","2","3","4","5","6","7","e","c","p","s","d","q","y","n"] 
    
    ## @brief A variable to hold the last keystroke
    last_key = None # Make sure the last_key variable is empty
    
    for _m in Keys: # Loop through the keys to prepare the callback function
        keyboard.on_release_key(_m, _callback = kb_cb)
        
    # Create an object of the vendoTronTask
    vendo = vendoTronTask(False, balance)
    
    try:
        while True:
            next(vendo) # continuously run the generator

            
    except KeyboardInterrupt:
        print('\nCtrl-c detected. Goodbye')
            
    except StopIteration:
        print('\nTask has stopped yielding its state. Check your code.')
    






















