## @file 405Lab0x01_page.py
#  @brief Mainpage for the ME405 Lab 0x01
# 
## @page ME405_Lab0x01 405 Lab 0x01: VendoTron Finite State Machine
#  
#  The first lab for ME 405 was a re-introduction for Python (or an 
#  introduction for those that haven't had ME 305 virtually). This lab focused
#  on building a finite state machine (FSM) that simulated a vending machine.
#  The building blocks of the lab included using a Python generator to host
#  the FSM, as well as using collections (in my case, dictionaries) to hold
#  the balance in terms of quantities of each denomination (pennies, nickels,
#  etc...). \n\n
#  
#  The goal of this FSM was to allow the user to input money at any time, 
#  eject their money at any time, and select a drink when they wanted to. 
#  the FSM would keep track of their current balance, and when a drink was 
#  selected, it would ensure that the inputted balance could cover the cost
#  of the desired soda. The setup for all the user commands was with keyboard
#  interrupts using the keyboard module. If the user pressed 'c', 'p', 's', or 
#  'd',that would signify that they wanted either a Cuke, Popsi, Spryte, or Dr. 
#  Pepper, respectively. For ejecting money, the user would press the 'e'. In 
#  order to enter coins or bills, the user would press 0 - 7, which would 
#  represent pennies, nickels, dimes, quarters, $1, $5, $10, or $20, 
#  respectively. Along with keeping track of the transaction, it had
#  a built-in timer that would prompt the user to finish their purchase if 
#  they took too long. \n\n
#  
#  The source code for this project can be viewed online at my personal
#  BitBucket account, linked here: 
#  <a class="custom" href="https://bitbucket.org/MatthewFrostCP/me405/src/master/Lab0x01/405_Lab0x01.py" 
#  target="_blank" rel="noopener noreferrer">Lab0x01 Source Code</a> \n\n
#  
#  To find the locally-saved files, please use the following link to find
#  the mainscript for this lab:\n
#       \ref 405_Lab0x01.py \n\n
#  
#  
#  Finally, a state-transition diagram was required for this lab, and helped 
#  with implementation of the Python code itself. Figure 1 below shows the 
#  appropriate state-transition diagram for the lab.\n\n
#  
#  @image      html ME405_Lab1_FSM.jpg " " width = 50%
#  @details    \n <CENTER> **Figure 1.** FSM for VendoTron </CENTER> \n
#  
#  
#  Click here for the next lab, \ref 405_lab0x02
#  
#  
# 